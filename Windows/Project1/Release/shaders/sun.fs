#version 330 core
out vec4 FragColor;

in vec2 TexCoords;
in vec3 normal;
uniform sampler2D texture_diffuse1;
//uniform vec3 lightPos; 
//uniform vec3 lightColor;
in vec3 position;
void main()


{   // ambient
	vec3 objectcolor=texture(texture_diffuse1, TexCoords).xyz;
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * vec3(1.0,1.0,1.0);
  	
    // diffuse 
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(vec3(0.0,10.0,-3.0) -position);
    float diff = max(dot(lightDir, norm), 0.0);
    vec3 diffuse = diff * vec3(1.0,1.0,1.0);
            
    vec3 result = (ambient + diffuse) * objectcolor;
  //FragColor = vec4(result, 1.0);
   //FragColor = vec4(normal, 0.0);
    FragColor = texture(texture_diffuse1, TexCoords);
    //FragColor=vec4(0.0f,0.0f,1.0f,1.0f);
   
}
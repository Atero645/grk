#include "shader.h"
#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>
int  Shader::LoadVertexShader(std::string path)
{
	std::ifstream vertex_shader(path.c_str()); 
	if(vertex_shader.fail())
	{
		std::cout<<"Error opening vertex shader file!!"<<std::endl;
		return -1;
	}
	else
	{
		std::string str((std::istreambuf_iterator<char>(vertex_shader)),(std::istreambuf_iterator<char>()));
		vertexShader=str;
	}
		

	return 0;

}

int Shader::LoadFragmentShader(std::string path)
{
	std::ifstream fragment_shader(path.c_str()); 
	if(fragment_shader.fail())
	{
		std::cout<<"Error opening fragment shader file!!"<<std::endl;
		return -1;
	}
	else
	{
		std::string str((std::istreambuf_iterator<char>(fragment_shader)),(std::istreambuf_iterator<char>()));
		fragmentShader=str;
	}
		
	return 0;
}

int Shader::CompileShader()
{
	const char* vertex_source=vertexShader.c_str();
	GLuint VertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(VertexShader, 1, &vertex_source, NULL);
    glCompileShader(VertexShader);
    // Check for compile time errors
    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(VertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        return -1;
    }
    // Fragment shader
    const char* fragment_source=fragmentShader.c_str();
    GLuint FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(FragmentShader, 1, &fragment_source, NULL);
    glCompileShader(FragmentShader);
    // Check for compile time errors
    glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(FragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
        return -1;
    }
    // Link shaders
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, VertexShader);
    glAttachShader(shaderProgram, FragmentShader);
    glLinkProgram(shaderProgram);
    // Check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        return -1;
    }
    glDeleteShader(VertexShader);
    glDeleteShader(FragmentShader);
    return 0;
}

void Shader::Use()
{
	glUseProgram(shaderProgram);
}

void Shader::setUniformMatrix4(const GLchar *name, const glm::mat4 &matrix)
{
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name), 1, GL_FALSE, glm::value_ptr(matrix));
}
void Shader::setUniformVec3f(const GLchar *name, const glm::vec3 &value)
{
	 glUniform3f(glGetUniformLocation(shaderProgram, name), value.x, value.y, value.z);
}
void Shader::setInt(const std::string &name, int value)
    {
        glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), value);
    }

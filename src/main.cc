#define GLEW_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "camera.h"
#include "stb_image.h"
//#include "Render_Utils.h"
#include "model.h"
#include "object.h"
#include <irrKlang.h>
using namespace std;


struct SkyBoxData {
	int vao;
	Shader shader;
	int cubemap;
};
struct Data {
	int vao;
	Shader shader;
	int texture1;
	int texture2;
};
int lockCam=1;
const unsigned int SCR_WIDTH = 1366;
const unsigned int SCR_HEIGHT = 768;

GLFWwindow* window;
int w, h;
glm::vec3 playerPos;
Camera camera(glm::vec3(0.0f, 0.0f, 4000.0f));

float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;

bool firstMouse = true;
float xoffset=0;
float yoffset=0;
// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;
void key_callback(GLFWwindow* window, int key, int scancode, int action,
		int mode);

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	if (firstMouse) {
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}



	 xoffset = xpos - lastX;
	 yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	camera.ProcessMouseScroll(yoffset);
}
unsigned int loadCubemap(vector<std::string> faces) {
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++) {
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height,
				&nrChannels, 0);
		if (data) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width,
					height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		} else {
			std::cout << "Cubemap texture failed to load at path: " << faces[i]
					<< std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}
int Init() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", nullptr,
			nullptr);
	if (window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	glfwGetFramebufferSize(window, &w, &h);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

SkyBoxData LoadSkybox() {
	float skyboxVertices[] = {
			// positions
			-1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f,

			-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f,

			1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f,

			-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f,

			-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f,

			-1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f };
	Shader skyboxShader;
	skyboxShader.LoadVertexShader("./shaders/skyboxShader.v");
	skyboxShader.LoadFragmentShader("./shaders/skyboxShader.f");
	skyboxShader.CompileShader();
	unsigned int skyboxVAO, skyboxVBO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices,
			GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
			(void*) 0);

	vector < std::string > faces { "./bkg/blue/right.png", "./bkg/blue/left.png",
			"./bkg/blue/top.png", "./bkg/blue/bot.png", "./bkg/blue/front.png",
			"./bkg/blue/back.png" };
	unsigned int cubemapTexture = loadCubemap(faces);
	skyboxShader.Use();
	skyboxShader.setInt("skybox", 0);
	SkyBoxData res;
	res.vao = skyboxVAO;
	res.shader = skyboxShader;
	res.cubemap = cubemapTexture;
	return res;

}
void SkyboxDraw(int vao, Shader skyboxShader, int cubemapTexture) {
	//glBindVertexArray(0);
	// draw skybox as last

	glm::mat4 projection = glm::perspective(glm::radians(45.0f),
			float(w) / float(h), 100.f, 1000000.0f);
	glm::mat4 view = glm::mat4(glm::mat3(camera.GetViewMatrix()));
	glDepthFunc (GL_LEQUAL); // change depth function so depth test passes when values are equal to depth buffer's content
	skyboxShader.Use();
	// remove translation from the view matrix
	skyboxShader.setUniformMatrix4("view", view);
	skyboxShader.setUniformMatrix4("projection", projection);
	// skybox cube
	glBindVertexArray(vao);
	glActiveTexture (GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glDepthFunc (GL_LESS);
}
Data LoadCube() {
	Shader shader;

	shader.LoadVertexShader("./shaders/vertexShader");
	shader.LoadFragmentShader("./shaders/fragmentShader");
	shader.CompileShader();
	float vertices[] = { -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f, -0.5f, -0.5f,
			1.0f, 0.0f, 0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f, 0.5f, -0.5f, 1.0f,
			1.0f, -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f,
			0.0f,

			-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
			0.5f, 0.5f, 1.0f, 1.0f, 0.5f, 0.5f, 0.5f, 1.0f, 1.0f, -0.5f, 0.5f,
			0.5f, 0.0f, 1.0f, -0.5f, -0.5f, 0.5f, 0.0f, 0.0f,

			-0.5f, 0.5f, 0.5f, 1.0f, 0.0f, -0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
			-0.5f, -0.5f, -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
			-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, -0.5f, 0.5f, 0.5f, 1.0f, 0.0f,

			0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
			-0.5f, -0.5f, 0.0f, 1.0f, 0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
			-0.5f, 0.5f, 0.0f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f, 0.0f,

			-0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f, -0.5f, -0.5f, 1.0f, 1.0f,
			0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.5f, -0.5f, 0.5f, 1.0f, 0.0f, -0.5f,
			-0.5f, 0.5f, 0.0f, 0.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,

			-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
			0.5f, 0.5f, 1.0f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f, 0.0f, -0.5f, 0.5f,
			0.5f, 0.0f, 0.0f, -0.5f, 0.5f, -0.5f, 0.0f, 1.0f };

	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
			(void*) 0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
			(void*) (3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// load and create a texture
	// -------------------------
	unsigned int texture1, texture2;
	// texture 1
	// ---------
	glGenTextures(1, &texture1);
	glBindTexture(GL_TEXTURE_2D, texture1);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char *data = stbi_load("./textures/container.jpg", &width, &height,
			&nrChannels, 0);
	if (data) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
				GL_UNSIGNED_BYTE, data);
		glGenerateMipmap (GL_TEXTURE_2D);
	} else {
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
	// texture 2
	// ---------
	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2);
	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	data = stbi_load("./textures/awesomeface.png", &width, &height, &nrChannels,
			0);
	if (data) {
		// note that the awesomeface.png has transparency and thus an alpha channel, so make sure to tell OpenGL the data type is of GL_RGBA
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA,
				GL_UNSIGNED_BYTE, data);
		glGenerateMipmap (GL_TEXTURE_2D);

	} else {
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);

	// tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
	// -------------------------------------------------------------------------------------------
	shader.Use();
	shader.setInt("texture1", 0);
	shader.setInt("texture2", 1);

	Data res;
	res.vao = VAO;
	res.shader = shader;
	res.texture1 = texture1;
	res.texture2 = texture2;
	return res;

}
void CubeDraw(int vao, Shader shader, int texture1, int texture2) {
	glActiveTexture (GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture1);
	glActiveTexture (GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture2);

	shader.Use();
	glm::mat4 model;
	model = glm::translate(model, glm::vec3(50.0f, 0.0f, 0.0f));
	model = glm::rotate(model, (float) glfwGetTime() * glm::radians(50.0f),
			glm::vec3(0.5f, 1.0f, 0.0f));
	glm::mat4 view;

	view = camera.GetViewMatrix();
	glm::mat4 projection;
	projection = glm::perspective(glm::radians(45.0f), float(w) / float(h),
			0.1f, 1000000.0f);
	shader.setUniformMatrix4("projection", projection);
	shader.setUniformMatrix4("view", view);
	shader.setUniformMatrix4("model", model);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
}

void calculateOrbit(Object *object, float orbit_time, float orbit_radius,
		glm::vec3 pos) {
	float angle = glfwGetTime() * 3.1419f / orbit_time;
	glm::vec3 pp;		    	            //=Planet.getPos(); //new position

	pp.x = sin(angle) * orbit_radius;
	pp.y = 0;//cos(angle) * orbit_radius;
	//printf("x %d y %d z %d\n",pp.x,pp.y,pp.z);
	pp.z = cos(angle) * orbit_radius;

	//pp.y+=0.5*deltaTime;

	object->setPos(pp + pos);
	//Planet2.setPos(pp2+Planet.getPos());
}

int main() {
	Init();
	irrklang::ISoundEngine* engine = irrklang::createIrrKlangDevice();

	glEnable(GL_MULTISAMPLE);
	glEnable (GL_DEPTH_TEST);

	SkyBoxData sky = LoadSkybox();
	Data cube = LoadCube();

	Shader sunShader;
	sunShader.LoadVertexShader("./shaders/sun.vs");
	sunShader.LoadFragmentShader("./shaders/sun.fs");
	sunShader.CompileShader();
	Shader ourShader;
	ourShader.LoadVertexShader("./shaders/1.vs");
	ourShader.LoadFragmentShader("./shaders/2.fs");
	ourShader.CompileShader();
	Shader ourShader2;
		ourShader2.LoadVertexShader("./shaders/1.vs");
		ourShader2.LoadFragmentShader("./shaders/3.fs");
		ourShader2.CompileShader();
	printf("OK\n");

	Object Sun("./objects/sun/sun.obj", sunShader, glm::vec3(0.0f, 0.f, 0.0f),
			glm::vec3(200.0f, 200.0f, 200.0f), &camera,
			glm::vec3(0.0f, 0.f, 0.0f));
	Object Blue("./objects/blue/blue.obj", ourShader2, glm::vec3(0.0f, 0.f, 0.0f),glm::vec3(10.0f, 10.0f, 10.0f), &camera,Sun.getPos());
	Object Planet("./objects/MarsPlanet/MarsPlanet.obj", ourShader2,glm::vec3(0.f, 0.0f, 0.0f), glm::vec3(70.f, 70.f, 70.f), &camera,Sun.getPos());
	Object Ship("./objects/tyderium/tyderium.obj", ourShader2,
			glm::vec3(40.0f, 20.0f, 4000.0f), glm::vec3(0.01f, 0.01f, 0.01f),
			&camera, Sun.getPos());
	Object Planet2("./objects/Moon/moon.obj", ourShader2, Planet.getPos(),
			glm::vec3(10.1f, 10.1f, 10.1f), &camera, Sun.getPos());
	playerPos=camera.Position-glm::vec3(0,50,60);
	Object Falcon("./objects/falcon/falcon.obj", ourShader2, playerPos,
			glm::vec3(10.f, 10.f, 10.f), &camera, Sun.getPos());

	Object ExplodingPlanet("./objects/explodingPlanet/Termanation.obj", ourShader2, glm::vec3(300.f, 300.f, 300.f), glm::vec3(100.0f, 100.0f, 100.0f), &camera,
			Sun.getPos());
	Object Tsar("./objects/tsar/TARDIS-FIGR_mkIII_station.obj",ourShader2,glm::vec3(-1000.f, 0.5f, -8000.f),glm::vec3(400.0f, 400.0f, 400.0f), &camera,
			Sun.getPos());
	Tsar.setAngle(65);
	Object Sovereign("./objects/sovereign/sovereign.obj",ourShader2,glm::vec3(600.f, 200.5f, 3200.f),glm::vec3(50.0f, 50.0f, 50.0f), &camera,
				Sun.getPos());

	Object Imperial("./objects/destroyer/imperial.obj",ourShader2,glm::vec3(1500.f, 0.5f, -5200.f),glm::vec3(200.0f, 200.0f, 200.0f), &camera,
					Sun.getPos());
	Imperial.setAngle(0);
	Imperial.Move(glm::vec3(1500.f,0.5f,60000.f),50);

	Object Imperial2("./objects/destroyer/imperial.obj",ourShader2,glm::vec3(2500.f, 0.5f, -5200.f),glm::vec3(200.0f, 200.0f, 200.0f), &camera,
					Sun.getPos());
		Imperial2.setAngle(0);
		Imperial2.Move(glm::vec3(2500.f,0.5f,60000.f),70);

	Object Mercury("./objects/mercury/mercury.obj",ourShader2,glm::vec3(300.f, 0.5f, 3000.f),glm::vec3(20.0f, 20.0f, 20.0f), &camera,
			Sun.getPos());


	Object TestSun("./objects/testSun/jojojoy_sun.mtl.obj",ourShader2,glm::vec3(200.f, 0.5f, 2000.f),glm::vec3(20.0f, 20.0f, 20.0f), &camera,
			Sun.getPos());
	Object Star("./objects/star/death-star-II.obj",ourShader2,glm::vec3(-1500.f, -400.f, 3200.f),glm::vec3(10.0f, 10.0f, 10.0f), &camera,
							Sun.getPos());
		Star.setAngle(180);
	Object Asteroid("./objects/asteroid/asteroid.obj",ourShader2,glm::vec3(200.f, 0.5f, 2000.f),glm::vec3(300.0f, 300.0f, 300.0f), &camera,
			Sun.getPos());
	Object Asteroid2("./objects/asteroid/asteroid.obj",ourShader2,glm::vec3(0.f, 0.f, 0.f),glm::vec3(80000.0f, 80000.0f, 80000.0f), &camera,
				Sun.getPos());


	engine->play2D("./ambient.mp3",true);
	while (!glfwWindowShouldClose(window)) {

		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		CubeDraw(cube.vao, cube.shader, cube.texture1, cube.texture2);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		calculateOrbit(&Blue,10,600,Sun.getPos());
		calculateOrbit(&Planet, 50, 15000, Sun.getPos());
		calculateOrbit(&Planet2, 10, 1600, Planet.getPos());
		calculateOrbit(&ExplodingPlanet, 60, 2000, Sun.getPos());
		calculateOrbit(&Asteroid,60,100,Mercury.getPos());
		Planet.setRotation(glm::vec3(0.5f, 1.0f, 0.0f),10);
		Planet2.setRotation(glm::vec3(0.5f, 1.0f, 0.0f),10);
		Mercury.setRotation(glm::vec3(0.5f, 1.0f, 0.0f),10);
		Asteroid.setRotation(glm::vec3(0.5f, 1.0f, 0.0f),10);
		Asteroid2.setRotation(glm::vec3(0.5f, 1.0f, 0.0f),1);
		Blue.Draw();
		Planet.Draw();
		Ship.Draw();
		Sun.Draw();
		Planet2.Draw();
		ExplodingPlanet.Draw();
		Mercury.Draw();
		Asteroid.Draw();
		if(lockCam){
		playerPos=camera.Position-glm::vec3(0,80,200);
		Falcon.setPos(playerPos);
		Falcon.setRot(xoffset,0);
		}
		Sovereign.Draw();
		Falcon.Draw();
		TestSun.Draw();
		Tsar.setRotation(glm::vec3(0.f,0.f,1.f),10.0);
		Tsar.Draw();
		Imperial.Draw(deltaTime);
		Star.Draw();
		Imperial2.Draw(deltaTime);
		Asteroid2.Draw();
		//Earth.Draw();
		SkyboxDraw(sky.vao, sky.shader, sky.cubemap);

		glfwSwapBuffers(window);
		xoffset=0;
		yoffset=0;
		glfwPollEvents();

	}
	engine->drop();
	glfwTerminate();

	return 0;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action,
		int mode) {
	// When a user presses the escape key, we set the WindowShouldClose property to true,
	// closing the application
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
			camera.ProcessKeyboard(UP, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
				camera.ProcessKeyboard(DOWN, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
					(lockCam)?(lockCam=0):(lockCam=1);

}

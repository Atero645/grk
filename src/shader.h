#ifndef __shader__
#define __shader__
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
 private:
 	//GLuint shaderProgram;
 	std::string vertexShader;
 	
 	std::string fragmentShader;
 public:
 	GLuint shaderProgram;
 	Shader(){};
 	int LoadVertexShader(std::string path);
 	int LoadFragmentShader(std::string path);
 	int CompileShader();
 	void Use();
 	void setUniformVec3f(const GLchar *name, const glm::vec3 &value);
 	void setUniformMatrix4(const GLchar *name, const glm::mat4 &matrix);
 	void setInt(const std::string &name, int value);
};
#endif

#ifndef __OBJECT__
#define __OBJECT__
#include "model.h"
#include "shader.h"
#include <glm/glm.hpp>
#include "camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
class Object
{
	Model *model3d;
	Shader shader;
	glm::vec3 Pos;
	glm::vec3 scale;
	Camera *camera;
	int SCR_HEIGHT;
	int SCR_WIDTH;
	float Pitch;
	float Yaw;
	glm::vec3 rotation_vector;
	float rotation_speed=0;
	glm::vec3 lightPos;
	glm::vec3 DEST;
	float Speed;
	float Angle;
public:
	Object(const char *model_path,Shader sh,glm::vec3 p,glm::vec3 s, Camera *cam,glm::vec3 light)
	{
		model3d=new Model(model_path);
		shader=sh;
		Pos=p;
		scale=s;
		camera=cam;
		SCR_WIDTH=1366;
		SCR_HEIGHT=768;
		lightPos=light;
		Pitch=0;
		Yaw=0;
		rotation_speed=0;
		DEST=Pos;
		Angle=0;
	}
	void setPos(glm::vec3 p)
	{
		Pos=p;
	}
	void setScale(glm::vec3 s)
	{
		scale=s;
	}
	void setRotation(glm::vec3 v,float speed)
	{
		rotation_vector=v;
		rotation_speed=speed;
	}
	void setShader(Shader s)
	{
		shader=s;
	}
	glm::vec3 getPos()
	{
		return Pos;
	}
	void setRot(float p,float y)
	{
		Pitch+=p*0.2;
		Yaw+=y*0.1;
		if (Pitch > 89.0f)
		                Pitch = 89.0f;
		            if (Pitch < -89.0f)
		                Pitch = -89.0f;
		          //  if(Pitch!=0)
		            	//Pos.z+=5;
	}
	void Move(glm::vec3 dst,float speed)
	{
		DEST=dst;
		Speed=speed;


	}
	void setAngle(float a)
	{
		Angle=a;
	}
	void Draw(float dt=0)
	{
		shader.Use();
		// view/projection transformations
		glm::mat4 projection = glm::perspective(glm::radians(camera->Zoom),
				(float) SCR_WIDTH / (float) SCR_HEIGHT, 8.0f, 1000000.0f);
		glm::mat4 view = camera->GetViewMatrix();
		//view= glm::rotate(view, glm::radians(-Pitch),glm::vec3(0.f, 1.0f, 0.0f)); // translate it down so it's at the center of the scene
				//view = glm::rotate(view, glm::radians(Yaw),glm::vec3(1.f, 0.0f, 0.0f));
		shader.setUniformMatrix4("projection", projection);
		shader.setUniformMatrix4("view", view);
		shader.setUniformVec3f("viewPos",camera->Position);

		if(DEST!=Pos)
		{
			if(Pos.x<DEST.x && glm::distance(Pos.x,DEST.x)>10)
				Pos.x+=Speed*dt;
			else if(Pos.x>DEST.x && glm::distance(Pos.x,DEST.x)>10)
				Pos.x-Speed*dt;

			if(Pos.y<DEST.y && glm::distance(Pos.y,DEST.y)>10)
							Pos.y+=Speed*dt;
						else if(Pos.y>DEST.y && glm::distance(Pos.y,DEST.y)>10)
							Pos.y-Speed*dt;
			if(Pos.z<DEST.z && glm::distance(Pos.z,DEST.z)>10)
							Pos.z+=Speed*dt;
						else if(Pos.z>DEST.z && glm::distance(Pos.z,DEST.z)>10)
							Pos.z-=Speed*dt;
		}



		// render the loaded model
		glm::mat4 model;
		model = glm::translate(model, Pos);
		model=glm::rotate(model,glm::radians(Angle),glm::vec3(0.f, 1.0f, 0.0f));
		if(rotation_speed>0)
		model = glm::rotate(model, (float) glfwGetTime() * glm::radians(rotation_speed),
						rotation_vector);
		model = glm::rotate(model, glm::radians(-Pitch),glm::vec3(0.f, 1.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(Yaw),glm::vec3(1.f, 0.0f, 0.0f));



		model = glm::scale(model, scale);// it's a bit too big for our scene, so scale it down
		shader.setUniformMatrix4("model", model);
		shader.setUniformVec3f("lightPos",lightPos);

		model3d->Draw(shader);
	}
};
#endif
